# Iter per l'accertamento della condizione di disabilità in età evolutiva ai fini dell'inclusione scolastica

## Nuova richiesta
### FASE 1<!--fold-->
+ **La scuola**
   + dopo un adeguato periodo di osservazione e potenziamento individua difficoltà con un possibile rischio di **disturbo del neurosviluppo, agitazione psicomotoria o altri disturbi**
   + documenta e segnala alla famiglia la situazione
   + invita la famiglia a rivolgersi ad un servizio di Neuropsichiatria infantile per un percorso di valutazione
+ **La famiglia**
   + su sollecitazione della scuola o in autonomia richiede una visita presso la Neuropsichiatria 
   + la modalità sarà quella dell'"Impegnativa del SSN (impegnativa rossa)" con richiesta di **prima visita neuropsichiatra**

### FASE 2<!--fold-->

+ **Il Servizio di Neuropsichiatria**
   + accoglie la richiesta della famiglia secondo le specifiche procedure
   + al termine del percorso rilascia il Certificato Medico Diagnostico Funzionale (CDMF)

### FASE 3<!--fold-->

+ **La famiglia**
   + porta ad un medico abilitato (pediatra di base, medico di medicina generale, in alcuni casi lo stesso neuropsichiatra) il Certificato Medico Diagnostico Fuzionale (CDMF)
+ **Il medico**
   + compila sul sito www.inps.it il **certificato medico introduttivo** per richiesta di invalidità e Legge 104
   + consegna alla famiglia il certificato medico introduttivo e il relativo **numero identificativo**

### FASE 4<!--fold-->

+ **La famiglia**
   + fa domanda di accertamento dal sito www.inps.it in autonomia oppure tramite un CAF o un patronato
   + nel portale INPS sostegni, sussidi e indennità -> [Domanda invalidità civile e accertamento sanitario](https://www.inps.it/it/it/dettaglio-scheda.schede-servizio-strumento.schede-servizi.domanda-invalidita-civile-e-accertamento-sanitario-50004.accertamento-sanitario.html)
   + in fase di compilazione selezionare
     + invalidità
     + handicap ai sensi della Legge 05.02.1992, n. 104
     + inclusione scolastica (insegnante di sostegno) che si trova all'interno del paragrafo L. 104

### FASE 5<!--fold-->

+ **La medicina legale dell'ASST**
    + comunica, tramite telefonata o sms, la data dell'appuntamento della commissione di valutazione (unica sede presso Medicina Legale dell'Asst Lecco - via Tubi n. 43 Lecco)
+ **La famiglia**
    + si reca con il minore all'appuntamento con tutta la documentazione medica recente intestata al minore, oltre al Certificato Medico Diagnostico Funzionale (CDMF)

### FASE 6<!--fold-->

+ **L'inps**
   + invia tramite due raccomandate
      + il verbale di invalidità
      + la L. 104
* **La medicina legale dell'ASST**
   + invia per mail il *verbale di inclusione scolastica*

### FASE 7<!--fold-->

+ **La famiglia** consegna copia del verbale sintetico di accertamento
   + alla neuropsichiatria per la redazione del Profilo di Funzionamento
   + alla scuola per attivare la richiesta delle risorse (insegnante di sostegno)

### FASE 8<!--fold-->

+ **La famiglia** consegna il Profilo di Funzionamento
   + alla scuola per il suo completamento e per la redazione del PEI

## Rinnovo

### Verbale in situazione di art. 3 comma 3
+ la validità del verbale è estesa **fino al termine degli studi**
+ **Il servizio di Neuropsichiatria** predispone il Profilo di Funzionamento aggiornato (Diagnosi Funzionale se entro il 30/06/2024)

### Verbale in situazione di art. 3 comma 1
+ **La famiglia** riprende l'iter a partire dalla **fase 3**: un medico abilitato compila il Certificato Medico Introduttivo nel portale INPS

## Aggravamento

+ **La famiglia** riprende l'iter a partire dalla **fase 3**: un medico abilitato compila il Certificato Medico Introduttivo nel portale INPS specificando la variazione clinica che determina l'aggravamento

## Documenti utili<!--fold-->

+ [Indicazioni ASST Milano](https://www.serviziterritoriali-asstmilano.it/servizi/collegio-alunno-disabile-3/)
+ [Servizio INPS](https://www.inps.it/it/it/dettaglio-scheda.schede-servizio-strumento.schede-servizi.domanda-invalidita-civile-e-accertamento-sanitario-50004.accertamento-sanitario.html)
+ [Circolare ATS - febbraio 2024](https://usr.istruzionelombardia.gov.it/20240214prot7294/)
+ [Nota USR nuova procedura - maggio 2024](https://usr.istruzionelombardia.gov.it/20240502prot22182/)
